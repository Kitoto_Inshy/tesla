function Hamburger(size, stuffing) {
    if (!size) {
        throw new HamburgerException('Не заданий розмір бургеру');
    } else if (!stuffing) {
        throw new HamburgerException('Не задана добавка до бургеру');
    }
    this.size = size;
    this.stuffing = stuffing;
    this.topping = [];
};

Hamburger.SIZE_SMALL = { size: 'small', calories: 20, price: 50, };
Hamburger.SIZE_LARGE = { size: 'large', calories: 100, price: 40, };
Hamburger.STUFFING_CHEESE = { stuffing: 'cheese', calories: 10, price: 20, };
Hamburger.STUFFING_SALAD = { stuffing: 'salad', calories: 20, price: 5, };
Hamburger.STUFFING_POTATO = { stuffing: 'potato', calories: 15, price: 10, };
Hamburger.TOPPING_MAYO = { topping: 'mayo', calories: 20, price: 5, };
Hamburger.TOPPING_SPICE = { topping: 'spice', calories: 15, price: 0, };

Hamburger.prototype.addTopping = function (toppingInput) {

    for (let i = 0; i < this.topping.length; i++) {
        if (this.topping[i].topping === toppingInput.topping) {
            throw new HamburgerException('Добавка вже присутня в бутику');
        };
    };

    if (toppingInput.topping === 'spice' || toppingInput.topping === 'mayo') {
        this.topping.push(toppingInput);
    } else {
        throw new HamburgerException('Такої добавки немає!');
    };

};

Hamburger.prototype.removeTopping = function (toppingInput) {

    if (this.topping.indexOf(toppingInput) >= 0) {
        this.topping.splice(this.topping.indexOf(toppingInput), 1);
    } else {
        throw new HamburgerException('Така добавка не присутня в бутику');
    };

};

Hamburger.prototype.getToppings = function () {

    if (this.topping.length === 0) {
        throw new HamburgerException('Добавок не є');
    } else {
        return this.topping;
    };

};

Hamburger.prototype.getSize = function () {
    return this.size;
};

Hamburger.prototype.getStuffing = function () {
    return this.stuffing;
};

Hamburger.prototype.calculatePrice = function () {
    let price = this.size.price + this.stuffing.price;

    for (let i = 0; i < this.topping.length; i++) {
        price += this.topping[i].price;
    };

    return price;
};

Hamburger.prototype.calculateCalories = function () {
    let calories = this.size.calories + this.stuffing.calories;

    for (let i = 0; i < this.topping.length; i++) {
        calories += this.topping[i].calories;
    };

    return calories;
};

function HamburgerException(message) {
    this.name = "HamburgerException";
    this.message = message;

    if (Error.captureStackTrace) {
        Error.captureStackTrace(this, this.constructor);
    } else {
        this.stack = (new Error()).stack;
    };
};

const burger = new Hamburger(Hamburger.SIZE_LARGE, Hamburger.STUFFING_CHEESE);

try {
    burger.addTopping(Hamburger.TOPPING_MAYO);
    burger.addTopping(Hamburger.TOPPING_SPICE);
    console.log(burger.topping);
    burger.removeTopping(Hamburger.TOPPING_SPICE);
    console.log(burger.topping);
    burger.removeTopping(Hamburger.TOPPING_MAYO);
    burger.removeTopping(Hamburger.TOPPING_SPICE);
} catch (error) {
    console.log(error.name + ': ' + error.message);
}
