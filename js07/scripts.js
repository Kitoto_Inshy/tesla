fetch('https://swapi.co/api/films/')
    .then(response => response.json())
    .then(response => {
        response.results.forEach(function (item) {
            const id = item.episode_id;
            const title = item.title;
            const openingCrawl = item.opening_crawl;
            const characters = item.characters;

            const container = document.querySelector('.container');
            const loader = document.createElement('div');
            loader.classList.add('lds-ellipsis');
            loader.setAttribute('id', `item${id}`);
            loader.innerHTML = '<div></div><div></div><div></div><div></div>';

            const episode = document.createElement('div');
            episode.classList.add('element');
            container.prepend(episode);
            episode.innerHTML += `<h2 class='title'>${title}</h2>
            <h3 class='title-id'>EPISODE № ${id}</h3>
            <p class='text'>DESCRIPTION:<br> ${openingCrawl}</p>
            <h4 class='name name-bold ep${id} hide'>Charcters:</h4>`;
            episode.append(loader);

            const getCharacters = characters.map(url => fetch(url));
            Promise.all(getCharacters)
                .then(res => {
                    res.forEach(elem => {
                        elem.json()
                            .then(result => {
                                episode.innerHTML += `<p class='name name${id}'>${result.name}</p>`;
                                const loader = document.querySelector(`#item${id}`);
                                if (loader) loader.remove();
                            });
                    });
                });
        });
    });