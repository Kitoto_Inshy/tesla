document.addEventListener("DOMContentLoaded", function () {
    Post.getPostsFromServer();
    Post.createPost();
});

class Post {
    constructor(users, post) {
        let { userId, id, title, body } = post;
        this.userId = userId;
        this.id = id;
        this.title = title;
        this.body = body;
        this.users = users;
        this.name = this.users.find(elem => elem.id === userId).name;
        this.username = this.users.find(elem => elem.id === userId).username;
        this.useremail = this.users.find(elem => elem.id === userId).email;
    }

    static createPost() {
        const openModalBtn = document.querySelector('.open-modal');
        const closeModalBtn = document.querySelector('.close-modal');
        const postPost = document.querySelector('.twit-btn-post');
        const modal = document.querySelector('.twit-modal-wrap');

        const postTitle = document.querySelector('.modal-title');
        const postText = document.querySelector('.modal-text');
        const users = JSON.parse(localStorage.getItem('users'));

        openModalBtn.addEventListener('click', function () {
            modal.classList.remove('hide');
            postTitle.value = '';
            postText.value = '';
        });
        closeModalBtn.addEventListener('click', function () {
            modal.classList.add('hide');
        });
        window.addEventListener('click', function (event) {
            if (event.target === modal) {
                modal.classList.add('hide');
            }
        });
        postPost.addEventListener('click', function () {
            const post = {
                userId: 1,
                id: +localStorage.getItem('postId') + 1,
                title: postTitle.value,
                body: postText.value,
            };
            localStorage.setItem('postId', `${post.id}`);
            const newPost = new Post(users, post);
            newPost.showPost();
            modal.classList.add('hide');
            newPost.postNewPostToServer();
        });
    }

    showPost() {
        const addPoint = document.querySelector('.open-modal');
        const twit = document.createElement('div')
        twit.classList.add('twit-wrap');
        twit.innerHTML = `<div class="twit" id="card${this.id}">
        <h4 class="twit-author">
            <span class="author-name">${this.name}</span>
            <i class="fas fa-check-circle twit-icon"></i>
            <span class="author-username">@${this.username}</span>
            <span class="author-email">${this.useremail}</span>
        </h4>
        <h3 class="twit-title">${this.title}</h3>
        <textarea class="text-title hide"></textarea>
        <p class="twit-text">${this.body}</p>
        <textarea class="text-area hide"></textarea>
        <div class="twit-btns">
            <button class="twit-btn-edit btn">Edit</button>
            <button class="twit-btn-save btn hide">Save</button>
            <button class="twit-btn-delete btn">Delete</button>
        </div>
        </div>`;
        addPoint.after(twit);
        this.editPost();
        this.deletePost();
    }

    editPost() {
        const editBtn = document.querySelector(`#card${this.id} .twit-btn-edit`);
        const saveBtn = document.querySelector(`#card${this.id} .twit-btn-save`);
        const twitTitle = document.querySelector(`#card${this.id} .twit-title`);
        const twitTitleTextArea = document.querySelector(`#card${this.id} .text-title`);
        const twitText = document.querySelector(`#card${this.id} .twit-text`);
        const twitTextTextArea = document.querySelector(`#card${this.id} .text-area`);
        const postEditedPostToServerBinded = this.postEditedPostToServer.bind(this);
        editBtn.addEventListener('click', function () {
            twitTitleTextArea.value = twitTitle.innerHTML;
            twitTitleTextArea.classList.remove('hide');
            twitTitle.classList.add('hide');
            twitTextTextArea.value = twitText.innerHTML;
            twitTextTextArea.classList.remove('hide');
            twitText.classList.add('hide');
            editBtn.classList.add('hide');
            saveBtn.classList.remove('hide');
        });
        saveBtn.addEventListener('click', function () {
            twitTitle.innerHTML = twitTitleTextArea.value;
            twitTitleTextArea.classList.add('hide');
            twitTitle.classList.remove('hide');
            twitText.innerHTML = twitTextTextArea.value;
            twitTextTextArea.classList.add('hide');
            twitText.classList.remove('hide');
            editBtn.classList.remove('hide');
            saveBtn.classList.add('hide');
            postEditedPostToServerBinded(twitTitleTextArea.value, twitTextTextArea.value);
        })
    }

    deletePost() {
        const post = document.querySelector(`#card${this.id}`);
        const deleteBtn = document.querySelector(`#card${this.id} .twit-btn-delete`);
        const deletePostFromServerBinded = this.deletePostFromServer.bind(this);
        deleteBtn.addEventListener('click', function () {
            post.parentElement.remove();
            deletePostFromServerBinded();
        });
    }

    postEditedPostToServer(title, body) {
        fetch(`https://jsonplaceholder.typicode.com/posts/${this.id}`, {
            method: "PUT",
            body: JSON.stringify({
                id: `${this.id}`,
                title: `${title}`,
                body: `${body}`,
                userId: `${this.userId}`
            }),
            headers: {
                "Content-type": "application/json; charset=UTF-8"
            }
        });
    }

    postNewPostToServer() {
        fetch(`https://jsonplaceholder.typicode.com/posts/${this.id}`, {
            method: "POST",
            body: JSON.stringify({
                id: `${this.id}`,
                title: `${this.title}`,
                body: `${this.body}`,
                userId: `${this.userId}`
            }),
            headers: {
                "Content-type": "application/json; charset=UTF-8"
            }
        });
    }

    deletePostFromServer() {
        fetch(`https://jsonplaceholder.typicode.com/posts/${this.id}`, {
            method: "DELETE"
        });
    }

    static getPostsFromServer() {
        const usersRequest = fetch("https://jsonplaceholder.typicode.com/users")
            .then(response => response.json())
            .then(users => {
                localStorage.setItem('users', `${JSON.stringify(users)}`);
                return users;
            });
        const postsRequest = fetch("https://jsonplaceholder.typicode.com/posts")
            .then(response => response.json())
            .then(posts => {
                return posts;
            });
        Promise.all([usersRequest, postsRequest]).then(res => {
            const users = res[0];
            const posts = res[1];
            posts.forEach(elem => {
                const post = new Post(users, elem);
                post.showPost();
                localStorage.setItem('postId', `${post.id}`);
            });
        });
    }
}