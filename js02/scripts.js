class Hamburger {
    constructor(size, stuffing) {
        if (!size) {
            throw new HamburgerException('Не заданий розмір бургеру');
        } else if (!stuffing) {
            throw new HamburgerException('Не задана добавка до бургеру');
        };
        this.size = size;
        this.stuffing = stuffing;
        this.topping = [];
    }

    static SIZE_SMALL = { size: 'small', calories: 20, price: 50, };
    static SIZE_LARGE = { size: 'large', calories: 100, price: 40, };
    static STUFFING_CHEESE = { stuffing: 'cheese', calories: 10, price: 20, };
    static STUFFING_SALAD = { stuffing: 'salad', calories: 20, price: 5, };
    static STUFFING_POTATO = { stuffing: 'potato', calories: 15, price: 10, };
    static TOPPING_MAYO = { topping: 'mayo', calories: 20, price: 5, };
    static TOPPING_SPICE = { topping: 'spice', calories: 15, price: 0, };

    addTopping(toppingInput) {

        this.topping.forEach(elem => {
            if (elem.topping === toppingInput.topping) {
                throw new HamburgerException('Добавка вже присутня в бутику');
            };
        });

        if (toppingInput.topping === 'spice' || toppingInput.topping === 'mayo') {
            this.topping.push(toppingInput);
        } else {
            throw new HamburgerException('Такої добавки немає!');
        };

    }

    removeTopping(toppingInput) {

        if (this.topping.indexOf(toppingInput) >= 0) {
            this.topping.splice(this.topping.indexOf(toppingInput), 1);
        } else {
            throw new HamburgerException('Така добавка не присутня в бутику');
        };
    }

    getToppings() {
        if (this.topping.length === 0) {
            throw new HamburgerException('Добавок не є');
        } else {
            return this.topping;
        };
    }

    getSize() {
        return this.size;
    }

    getStuffing() {
        return this.stuffing;
    }

    calculatePrice() {

        let price = this.size.price + this.stuffing.price;

        this.topping.forEach(elem => price += elem.price);

        return price;

    }

    calculateCalories() {

        let calories = this.size.calories + this.stuffing.calories;

        this.topping.forEach(elem => calories += elem.calories);

        return calories;
    }
}

function HamburgerException(message) {
    this.name = "HamburgerException";
    this.message = message;
};

const burger = new Hamburger(Hamburger.SIZE_LARGE, Hamburger.STUFFING_POTATO);
console.log(burger);

try {
    burger.addTopping(Hamburger.TOPPING_SPICE);
    burger.addTopping(Hamburger.TOPPING_MAYO);
    burger.removeTopping(Hamburger.TOPPING_MAYO);
    burger.removeTopping(Hamburger.TOPPING_MAYO);
    console.log(burger);
} catch (error) {
    console.log(error.name + ': ' + error.message);
}