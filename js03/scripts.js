class GameLogic {

    activeCell = false;
    humanCounter = [];
    compCounter = [];

    constructor(time) {
        this.time = time;
    }

    createGameListener(event) {
        let $target = $(event.currentTarget);
        if ($target.hasClass('active')) {
            $target.removeClass('active').addClass('human-cell');
            this.humanCounter.push(+$target.attr('id'));
            $('.score-human-amount').text(`${this.humanCounter.length}`);
        } else if ($target.hasClass('human-cell') || $target.hasClass('computer-cell')) {

        }
    }

    addListenersToGameCells() {
        $('.game-table-cell').click(this.createGameListener.bind(this));
    }

    chooseRandomCellId() {
        let choosenId = false;
        let helpVar;
        do {
            helpVar = false;
            choosenId = Math.floor(Math.random() * 101);
            if (this.humanCounter.concat(this.compCounter).includes(choosenId)) {
                helpVar = true;
            };
        } while (helpVar);
        return choosenId;
    }

    chooseRandomCellInTable() {
        this.cleanField();
        $('.game-table-cell').removeClass('active');
        let cellId;
        let timer = setInterval(() => {
            if (!$(`#${this.activeCell}`).hasClass('human-cell') && this.activeCell) {
                $(`#${this.activeCell}`).removeClass('active').addClass('computer-cell');
                this.compCounter.push(this.activeCell);
                $('.score-computer-amount').text(`${this.compCounter.length}`);
            };
            if (this.compCounter.length + this.humanCounter.length >= 50) {
                clearInterval(timer);
                this.endGame();
            };
            cellId = this.chooseRandomCellId();
            $(`#${cellId}`).addClass('active');
            this.activeCell = cellId;
        }, this.time);
    }

    startGame() {
        this.addListenersToGameCells();
        this.chooseRandomCellInTable();
        $('.btn').addClass('hide');
        $('.game-btns-title').addClass('hide');
        $('.fight').removeClass('hide');
    }

    endGame() {
        if (this.compCounter.length > this.humanCounter.length) {
            alert('CompuKter wins');
            $('.btn').removeClass('hide');
            $('.game-btns-title').removeClass('hide');
            $('.fight').addClass('hide');
        } else if (this.compCounter.length < this.humanCounter.length) {
            alert('Leather bastard wins');
            $('.btn').removeClass('hide');
            $('.game-btns-title').removeClass('hide');
            $('.fight').addClass('hide');
        } else {
            alert('Human === Computer');
            $('.btn').removeClass('hide');
            $('.game-btns-title').removeClass('hide');
            $('.fight').addClass('hide');
        }
        this.cleanField();
    }

    cleanField() {
        this.compCounter.length = 0;
        this.humanCounter.length = 0;
        $('.score-computer-amount').text(`${this.compCounter.length}`);
        $('.score-human-amount').text(`${this.humanCounter.length}`);
        $('.game-table-cell').addClass('active')
            .removeClass('human-cell')
            .removeClass('computer-cell');
    }
}

$('.btn-easy').click(function () {
    const game = new GameLogic(1500);
    game.startGame();
});

$('.btn-norm').click(function () {
    const game = new GameLogic(1000);
    game.startGame();
});

$('.btn-hard').click(function () {
    const game = new GameLogic(500);
    game.startGame();
});