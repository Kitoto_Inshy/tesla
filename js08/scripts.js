const btn = document.querySelector('.action-btn');
btn.addEventListener('click', getInfo);

async function getInfo() {
    const ipQuery = await fetch('https://api.ipify.org/?format=json');
    const ipResponce = await ipQuery.json();

    const infoQuery = await fetch(`http://ip-api.com/json/${ipResponce.ip}?fields=continent,country,region,city,district&lang=ru`);
    const infoResponce = await infoQuery.json();
    const info = document.createElement('div');
    info.innerHTML = `
    <p class="continent">Континент: ${infoResponce.continent}</p>
    <p class="country">Страна: ${infoResponce.country}</p>
    <p class="region">Регион: ${infoResponce.region}</p>
    <p class="city">Город: ${infoResponce.city}</p>
    <p class="district">Район: ${infoResponce.district}</p>`
    btn.after(info);
};