let gulp = require('gulp'),
	sass = require('gulp-sass'),
	sync = require('browser-sync');

gulp.task('sass', function() {
	return gulp.src(['src/sass/**/*.sass', 'src/sass/**/*.scss'])
		.pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError))
		.pipe(gulp.dest('./dist/css'))
		.pipe(sync.reload({stream: true}))
});

gulp.task('html', function() {
	return gulp.src('./dist/*.html')
		.pipe(sync.reload({stream: true}))
})

gulp.task('watch', function() {
	gulp.watch(['src/sass/**/*.sass', 'src/sass/**/*.scss'], gulp.series('sass'));
	gulp.watch('./dist/*.html', gulp.series('html'));  
});

gulp.task('sync', function() {
	sync.init({
		server: {
			baseDir: 'dist'
		}
	})
})

gulp.task('default', gulp.parallel('sync', 'watch'));