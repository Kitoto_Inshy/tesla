const request = new XMLHttpRequest();
request.open('GET', 'https://swapi.co/api/films/');
request.type = 'json';
request.send();

request.onload = function () {
    const obj = JSON.parse(request.response);
    obj.results.forEach(function (item) {
        prtElementsArr(item.characters, item.episode_id, item.title, item.opening_crawl);
    });
};

function prtElementsArr(arr, item1, item2, item3) {
    const container = document.querySelector('.container');

    const loader = document.createElement('div');
    loader.classList.add('lds-ellipsis');
    loader.setAttribute('id', `item${item1}`);
    loader.innerHTML = '<div></div><div></div><div></div><div></div>';

    const episode = document.createElement('div');
    episode.classList.add('element');
    container.prepend(episode);
    episode.innerHTML += `<h2 class='title'>${item2}</h2>
    <h3 class='title-id'>EPISODE № ${item1}</h3>
    <p class='text'>DESCRIPTION:<br> ${item3}</p>
    <h4 class='name name-bold ep${item1} hide'>Charcters:</h4>`;
    episode.append(loader);

    let count = 0;

    arr.forEach(elem => {

        const charRequest = new XMLHttpRequest();
        charRequest.open('GET', elem);
        charRequest.type = 'json';
        charRequest.send();

        charRequest.onload = function () {
            ++count;
            if (count === arr.length) {
                const loader = document.querySelector(`#item${item1}`);
                loader.remove();
                const text = document.querySelectorAll(`.name${item1}`);
                text.forEach(elem => elem.classList.remove('hide'));
                const charTitle = document.querySelectorAll(`.ep${item1}`);
                charTitle.forEach(elem => elem.classList.remove('hide'));
            };
            const obj = JSON.parse(charRequest.response);
            episode.innerHTML += `<p class='name name${item1} hide'>${obj.name}</p>`;
        };
    });
};